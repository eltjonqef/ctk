import "./Current.css";
import day from "./Day";
import React, { useState, useEffect } from "react";
import weather from "./weather";
class Current extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      res: weather
    };
  }
  componentDidMount() {
    // Simple GET request using fetch
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?q=${this.props.city}&appid=7a5a0dd163cb1e8891c0c42885b92ae0&units=metric`
    )
      .then((response) => response.json())
      .then((data) => this.setState({ res: data }));
  }

  //   let img = `http://openweathermap.org/img/wn/${res.weather[0].icon}@2x.png`;
  // const {res}=props;
  render() {
    let res = this.state.res;
    //console.log(res.weather[0]);
    return (
      <div class="current">
        <div class="weather">
          <img src={`http://openweathermap.org/img/wn/${res.weather[0].icon}@2x.png`}></img>
          <span id="temp">{res.main.temp}</span>
          <span id="unit">&#176;</span>
        </div>
        <div class="additional">
          <span>
            <nobr>Humidity: {res.main.humidity}%</nobr>
          </span>
          <span>
            <nobr>Wind: {res.wind.speed}m/s</nobr>
          </span>
        </div>
        <div class="info">
          <span id="city">{res.name}</span>
          <span id="day">{day[new Date(res.dt * 1000).getDay()]}</span>
          <span id="description">{res.weather[0].main}</span>
        </div>
      </div>
    );
  }
}

export default Current;
