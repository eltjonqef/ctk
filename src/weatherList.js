let weatherList={res:{
    dt: 1568977200,
    sunrise: 1568958164,
    sunset: 1569002733,
    temp: {
      day: 293.79,
      min: 288.85,
      max: 294.47,
      night: 288.85,
      eve: 290.44,
      morn: 293.79,
    },
    feels_like: {
      day: 278.87,
      night: 282.73,
      eve: 281.92,
      morn: 278.87,
    },
    pressure: 1025.04,
    humidity: 42,
    weather: [
      {
        id: 800,
        main: "Clear",
        description: "sky is clear",
        icon: "01d",
      },
    ],
    speed: 4.66,
    deg: 102,
    gust: 5.3,
    clouds: 0,
    pop: 0.24,
  }};

  export default weatherList;