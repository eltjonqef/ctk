import { render } from '@testing-library/react';
import { ready } from 'jquery';
import React from 'react';
import Box from './Box';
import './Row.css';
import weatherPre from './weatherPre';
class Row extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          res: {
            list:[
              {},{},{},{},{},{},
            ]
          },
          ready: false
        };
      }

      componentDidMount() {
        // Simple GET request using fetch
        fetch(
          `http://api.openweathermap.org/data/2.5/forecast?q=${this.props.city}&appid=7a5a0dd163cb1e8891c0c42885b92ae0&units=metric`
        )
          .then((response) => response.json())
          .then((data) => this.setState({ 
            res: data,
            ready: true
          }));
      }
    
    render(){
      console.log(this.state.res);
        let boxes=[ 
            <Box res={this.state.res.list[1]}/>,
            <Box res={this.state.res.list[2]}/>,
            <Box res={this.state.res.list[3]}/>,
            <Box res={this.state.res.list[4]}/>,
            <Box res={this.state.res.list[5]}/>,
            <Box res={this.state.res.list[6]}/>,
            <Box res={this.state.res.list[7]}/>,
            <Box res={this.state.res.list[8]}/>,
            <Box res={this.state.res.list[9]}/>,
        ];
        console.log("state" + this.state.ready);
        if(!this.state.ready)
          return <div>lul</div>
        return (
            <div class="boxes">
                {boxes}
            </div>
        );
    }
}

export default Row;