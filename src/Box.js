import "./Box.css";
import WeatherID from "./WeatherID";
import day from "./Day";
import Current from "./Currect";
import ReactDOM from "react-dom";
import weatherPre from "./weatherPre";
import React, { useState, useEffect } from "react";
import weatherList from "./weatherList";

class Box extends React.Component {
  constructor(props) {
    super(props);

  }
 

  render() {
    const { res } = this.props;
      return (
        <div class="box">
          {day[new Date(res.dt * 1000).getDay()]}
          <img
            src={`http://openweathermap.org/img/wn/${res.weather[0].icon}@2x.png`}
          ></img>
          <div class="temp">
            <span id="max">{res.main.temp_max}&#176;</span>&nbsp;&nbsp;
            <span id="min">{res.main.temp_min}&#176;</span>
          </div>
        </div>
      );
  }
}

export default Box;
