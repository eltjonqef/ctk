import Row from './Row';
import Current from './Currect';
import weather from './weather';
import weatherPre from './weatherPre';
import React from 'react';
class App extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {
          city: "Gothenburg"
        };
        this.searchCity = this.searchCity.bind(this);
    }
    searchCity(){
        this.setState({city:document.getElementById('cityInput').value});
        console.log("AA" +this.state.city);
    }
    render(){
        const {city}=this.state;
        return(
            <div>
                <input type="text" id="cityInput" placeholder='Enter City'></input>
                <button type='button' onClick={this.searchCity}>Search</button>
                <Current key={city} city={city}/>
                <Row key={city+"1"} city={city}/>
            </div>
        )

    }
}

export default App;